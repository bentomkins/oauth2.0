# OIDC Authorization Code Flow (with a Public Client) Architecture

<img src="OICD.png">

1. User launches SPA application (e.g. es-portal);
2. Browser requests static assets from Static Asset Server;
3. SPA initialises and initiates the user login process with the Authentication Library;
4. Browser makes initial call to Authorisation Endpoint;
5. Since the user is not yet authenticated, AuthZ redirects the browser the AutnN Service. The user enters credentials and is authenticated;
6. The AuthN Service redirects the browser back to the Authorisation End Point. This time, the user is authenticated and an authorisation code is returned to a preregistered Reditect URI;
7. The authentication library obtains the Authorisation code from the redirected endpoint (SPA reloaded from the redirect endoint and that sees the callback endpoint was called, which provides the Authorization Code to the Authentication Library);
8. The authentication library makes a call to the Token Endpoint to exchange the Authorisation Code for the required tokens (ID Token, Access Token, Refresh Token);
9. The Authentication Library validates the ID Token and caches the ID Token, Access Token, Refresh Token;
10. The SPA can make JSON payload API calls (with the access token and ID token) to the backend (API Gateway, or alternative);
11. The authenticated JSON payloads (using the ID token) are authenticated against to establish if your privileges will allow the call to be routed to the specified service;

## Terms and Definitions
* Identity: Unique name of person or device
* Authentication: Process of verifying identity
* Authorization: Function over identity assigning access rights. Grant access based on a set of rules, where the rules depend on identity.
* Access rights: Privileges for accessing data or calling functionality
* 
        <set-payload value="#[['One', 'Two', 'Three']]" doc:name="Set Payload" />
        
		<splitter expression="#[payload]" doc:name="Splitter" enableCorrelation="NEVER">
			<expression-message-info-mapping messageIdExpression="#[message.id]" correlationIdExpression="#[message.correlationId]"/>
		</splitter>
		<scatter-gather doc:name="Scatter-Gather">
			<custom-aggregation-strategy class="com.hsbc.CollectAllAggregationWithFlowVars"/>
			
			<processor-chain>
				<set-variable variableName="one" value="1: #[message.payload]" doc:name="set 1st variable to current splitter value"/>
				<logger message="log 1st in flow ---------&gt; #[flowVars.one]" level="INFO" doc:name="Logger"/>
			</processor-chain>
			<processor-chain>
				<set-variable variableName="two" value="2: #[message.payload]" doc:name="set 2nd variable to current splitter value"/>
				<logger message="log 2nd in flow ---------&gt; #[flowVars.two]" level="INFO" doc:name="Logger"/>
			</processor-chain>
			<processor-chain>
				<set-variable variableName="three" value="3: #[message.payload]" doc:name="set 3rd variable to current splitter value"/>
				<logger message="log 3rd in flow ---------&gt; #[flowVars.three]" level="INFO" doc:name="Logger"/>
			</processor-chain>
		</scatter-gather>
		<collection-aggregator failOnTimeout="true" doc:name="Collection Aggregator"/>
		
		<logger message="log 1st after scatter-gather --------&gt; #[flowVars.one]" level="INFO" doc:name="get 1st variable"/>
		<logger message="log 2nd after scatter-gather --------&gt; #[flowVars.two]" level="INFO" doc:name="get 2nd variable"/>
		<logger message="log 3rd after scatter-gather --------&gt; #[flowVars.three]" level="INFO" doc:name="get 3rd variable"/>		
        <set-payload value="Check Log " doc:name="Set Payload" mimeType="text/plain"/>